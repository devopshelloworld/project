<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>DevOps Hello World</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
			html, body {
				height: 100%;
				width: 100%;
			}

			body {
				background: linear-gradient(to bottom, #396afc, #2948ff); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
				display: flex;
				flex-direction: column;
				justify-content: center;
				align-items: center;
				margin: 0;
			}

			div {
				display: flex;
			}

			h1 {
				font-family: 'Roboto', sans-serif;
				color: white;
				font-weight: 400;
			}

			strong {
				font-weight: 700;
				font-size: 2em;
			}

			.love {
				color: red;
				margin-left: 20px;
				font-size: 3em;
			}

			.fancy-shadow {
				text-shadow: 0px 4px 3px rgba(0,0,0,0.4),
							0px 8px 13px rgba(0,0,0,0.1),
							0px 18px 23px rgba(0,0,0,0.1);
			}

			.center-elements {
				display: flex;
				flex-direction: column;
				align-items: center;
			}
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <h1 class="fancy-shadow center-elements">
										<strong>DevOps Hello World!!!</strong>
										<strong><a href="https://docs.google.com/presentation/d/e/2PACX-1vSvdgkP2WanhwAowNnA89ElvxQ21F0FjIoSehhiYKVFjCKR-aj5lXUsOKt2Gikysg5rehAXKt08wEet/pub?start=false&loop=false&delayms=3000&slide=id.p">Link para os slides</a></strong>
										<strong class="love">&hearts;</strong>
                </h1>
            </div>
        </div>
    </body>
</html>
